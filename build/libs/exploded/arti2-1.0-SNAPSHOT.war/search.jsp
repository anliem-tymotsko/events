<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="style.css" style="background-color: lightgray">

<head>
    <title>Blog</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<html>
<body style="background-image: url(http://www.ohphoto.com.au/gallery/original/Grey-gradient_3.jpg)">

<ul class="nav nav-tabs" style="background-color: #272125; text-decoration-color: whitesmoke">

    <ul class="nav nav-tabs" style="background-color: #272125; text-decoration-color: whitesmoke">
        <li class="nav-item ">
            <a class="nav-link " href="${pageContext.request.contextPath}/blog">BLOG</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link active" href="${pageContext.request.contextPath}/search">Search</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/addEvent">Add event</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1"
               href="${pageContext.request.contextPath}/SignIn">LogOut</a>
        </li>
    </ul>
</ul>
<form action="${pageContext.request.contextPath}/search" method="post" style="width : 320px;">
    <h1>Input fields </h1>
    <div></div>
    <h3>Event name</h3>
    <input type="text" class="form-control" name="name_find" placeholder="event...">
    </div>

    <div class="form-group">
        <div><h2>category</h2>
            <select name="id_category">
                <c:forEach items="${categories}" var="category">
                <option value="${category.id}">${category.name}</option>
                </c:forEach>
        </div>
        <div>     <div></div>
            <h2>description</h2>
            <input type="text" class="form-control" name="description" placeholder="text">
        </div>
    </div>
    <button type="submit">find</button>
</form>
</body>
</html>
