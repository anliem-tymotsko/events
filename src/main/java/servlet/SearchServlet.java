package servlet;

import model.Category;
import model.Event;
import service.CategoryServiceImp;
import service.EventServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@WebServlet(name = "SearchServlet", urlPatterns = "/search")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        EventServiceImpl eventService = new EventServiceImpl();
        List<Event> events = null;
        String name = request.getParameter("name_find");
        String descr = request.getParameter("description");
        if (!Objects.equals(request.getParameter("name_find"), "")) {
            try {
                events = eventService.findByName(name);
                if(!descr.equals(""))
                    events = events.stream().filter(e->e.getDescription().contains(descr)).collect(Collectors.toList());
                request.setAttribute("events", events);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("blog.jsp");
        requestDispatcher.include(request, response);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        CategoryServiceImp categoryServiceImp = new CategoryServiceImp();
        try {

            List<Category> categories = categoryServiceImp.getAll();
            request.setAttribute("categories", categories);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("search.jsp");
        requestDispatcher.include(request, response);


    }
}

